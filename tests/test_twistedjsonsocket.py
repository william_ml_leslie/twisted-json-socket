import sys
import unittest
import json

import twistedjsonsocket


def build_message(data):
    msg = json.dumps(data).encode("utf-8")
    return "%s#%s" % (len(msg), msg), data


def slice_message(msg, part_len=5):
    """
    Slices a message into parts of part_len.
    """
    while msg:
        yield msg[0:part_len]
        msg = msg[part_len:]


def dict_cmp(a, b):
    keys = set(a.keys() + b.keys())

    return not any([
        k for k in keys if a.get(k) != b.get(k)
    ])


class Transport:
    def __init__(self):
        self.messages = []

    def write(self, data):
        self.messages.append(data)


class Protocol(twistedjsonsocket.JSONSocketProtocol):
    def __init__(self):
        twistedjsonsocket.JSONSocketProtocol.__init__(self)
        self.inbox = []
        self.last_msg = None
        self.transport = Transport()

    def lineReceived(self, data):
        self.inbox.append(data)
        self.last_msg = data

    def exceptionHandling(self):
        exc_info = sys.exc_info()
        raise exc_info[0], exc_info[1], exc_info[2]


class TestProtocol(unittest.TestCase):
    simple_dict = {
        's': 'a string',
        'n': 101,
        'a': [1,2,3],
    }

    complex_dict = {
        'a': [simple_dict, simple_dict, simple_dict],
        'b': {
            'a': simple_dict,
            'b': simple_dict,
            'c': simple_dict
        }
    }

    unicode_dict = {
        u'\u2603' : simple_dict,
        'comet' : u'\u2604'
    }

    def setUp(self):
        self.protocol = Protocol()

    def tearDown(self):
        self.assertEquals(self.protocol._buffer, "")

    def test_simple_message(self):
        msg, data = build_message(self.simple_dict)

        self.protocol.dataReceived(msg)
        self.assertTrue(dict_cmp(self.protocol.inbox[0], data))

    def test_unicode(self):
        msg, data = build_message(self.unicode_dict)
        self.protocol.dataReceived(msg)
        self.assertTrue(dict_cmp(self.protocol.inbox[0], data))

    def test_unicode_send(self):
        self.protocol.sendData(self.unicode_dict)
        message = self.protocol.transport.messages[0]
        datalen, sep, data = message.partition("#")
        self.assertEquals(int(datalen), len(data))

    def test_fragmented_message(self):
        msg, data = build_message(self.simple_dict)

        for fragment_len in range(1, 20):
            fragments = slice_message(msg, fragment_len)

            for frag in fragments:
                self.assertEquals(len(self.protocol.inbox), 0)
                self.protocol.dataReceived(frag)

            self.assertEquals(len(self.protocol.inbox), 1)
            self.assertTrue(dict_cmp(self.protocol.inbox[0], data))
            self.assertEquals(self.protocol._buffer, "")
            self.protocol.inbox.pop(0)

    def test_invalid_data(self):
        with self.assertRaises(ValueError):
            self.protocol.dataReceived("5#/////")
